#! /usr/bin/perl -w

use strict;

sub percentile {
    my ($values, $percentile) = @_;
    my $total = scalar @$values;
    return 0 if $total == 0;
    my $index = ($total - 1) * $percentile;
    my $diff = $index - int($index);
    if ($index == int($index)) {
        return $values->[$index];
    } else {
        my $result = $values->[int($index)] + ($values->[int($index) + 1] - $values->[int($index)]) * $diff;
        return $result;
    }
}

#main
{
    my @sku_data;
    my %sku_map;
    my @sku_cycle_time;

    while (<>) {
        chomp;
        my ($uuid, $sku, $sku_no_size, $publish_date_key, $publish_date, $return_date_key, $return_date, $cycle_time)
            = split /;/, $_;
        my @sku_row = ($uuid, $sku, $sku_no_size, $publish_date_key, $publish_date, $return_date_key, $return_date, $cycle_time);
        next if ($cycle_time eq '');
        push @sku_data, \@sku_row;
        if (!defined $sku_map{$sku}) {
            $sku_map{$sku} = [];
        }
        push @{$sku_map{$sku}}, $cycle_time;
        push @sku_cycle_time, $cycle_time;
    }

    foreach my $sku (sort keys %sku_map) {
        my @values = sort { $a <=> $b } @{$sku_map{$sku}};
        my $p_65 = percentile(\@values, 0.65);
        my $p_75 = percentile(\@values, 0.75);
        my $p_85 = percentile(\@values, 0.85);
        my $p_95 = percentile(\@values, 0.95);
        my $p_25 = percentile(\@values, 0.25);
        my $p_50 = percentile(\@values, 0.50);
        my $total = scalar(@values);

        print "$sku,$p_65,$p_75,$p_85,$p_95,$p_25,$p_50,$total\n";
    }
}
